package controllers.service

import model.slack.api.chat.postMessage.ChatPostMessageConfiguration
import model.slack.bot.configuration.SlackBotConfiguration
import model.slack.bot.configuration.timerConfiguration.{OnceADayConfiguration, XTimesAHourConfiguration}
import model.slack.bot.reminderBot.ReminderBotConfiguration
import model.slack.bot.{SlackBot, SlackBotResult}
import model.slack.helper.TextCreateHelper
import org.joda.time.DateTime

import scala.collection._

object SlackBotsManager {
  // 状態変化するのでvar RepositoryOfMemoryみたいな感じかな
  private[this] var bots: Seq[SlackBot[_ <: SlackBotConfiguration, _ <: SlackBotResult]] = Seq(
    SlackBot(ReminderBotConfiguration("テスト用", XTimesAHourConfiguration(0, 15, 30, 45), ChatPostMessageConfiguration(
      text = TextCreateHelper("時間に数回テスト", Seq("y_harada")).createText()
    ),
      messageCustomFunction = c => c.copy(text = DateTime.now().toString("yyyy/MM/dd HH:mm") + c.text)
    )),
    SlackBot(ReminderBotConfiguration("朝会用", OnceADayConfiguration(9, 55), ChatPostMessageConfiguration(
      text = TextCreateHelper("<!channel>もうすぐ朝会ですよ").createText(),
      channel = "#p4f"
    ))),
    SlackBot(ReminderBotConfiguration("プルリクタイム用", OnceADayConfiguration(17, 25), ChatPostMessageConfiguration(
      text = TextCreateHelper("<!channel>もうすぐプルリクタイムですよ").createText(),
      channel = "#p4f"
    ))),
    SlackBot(ReminderBotConfiguration("夕会用", OnceADayConfiguration(17, 58), ChatPostMessageConfiguration(
      text = TextCreateHelper("<!channel>もうすぐ夕会ですよ").createText(),
      channel = "#p4f"
    )))
  )

  def settingBots: Seq[SlackBot[_ <: SlackBotConfiguration, _ <: SlackBotResult]] = bots

  def startAll(): Unit = {
    bots = bots.map(_.setSchedule())
  }

  def killAll(): Unit = {
    bots = bots.map(_.unsetSchedule())
  }

  def startById(id: String): Unit = {
    bots = bots.collect{
      case b if b.id == id => b.setSchedule()
      case other => other
    }
  }

  def killById(id: String): Unit = {
    bots = bots.collect{
      case b if b.id == id => b.unsetSchedule()
      case other => other
    }
  }

  def deleteById(id: String): Unit = {
    bots = bots.filter(_.id != id)
  }

  def appendBot(slackBot: SlackBot[_ <: SlackBotConfiguration, _ <: SlackBotResult]): Unit = {
    bots = bots.+:(slackBot)
  }
}
