package controllers

import controllers.service.SlackBotsManager
import model.sample.SampleService
import play.api.mvc._

class Application extends Controller {

  def index = Action {
    Ok(views.html.index(SlackBotsManager.settingBots))
  }

  def startAll = Action {
    SlackBotsManager.startAll()
    Redirect("/")
  }

  def killAll = Action {
    SlackBotsManager.killAll()
    Redirect("/")
  }

  def start(id:String) = Action {
    SlackBotsManager.startById(id)
    Redirect("/")
  }

  def kill(id:String) = Action {
    SlackBotsManager.killById(id)
    Redirect("/")
  }

  def webSocketLog = WebSocket.using[String] { request =>
    SampleService.attach("log")
  }
}
