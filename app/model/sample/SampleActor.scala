package model.sample

import akka.actor._
import org.joda.time.DateTime
import play.api.libs.iteratee.Concurrent

class SampleActor(id: String) extends Actor {

  private[this] val (enumerator, channel) = Concurrent.broadcast[String]

  def receive = {
    case Refresh(Some(s)) => dispatch(s"${DateTime.now.toString}:$s")
    case Refresh(None) => {}
    case Connect(host) => sender ! Connected(enumerator)
  }

  private def dispatch(message: String) = {
    channel.push(message)
  }
}