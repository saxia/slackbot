package model.sample

import akka.actor.{ActorRef, Props}
import akka.util.Timeout
import play.api.Play.current
import play.api.libs.concurrent.Akka
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.iteratee.{Enumerator, Iteratee}
import akka.pattern.ask

import scala.concurrent.Await
import scala.concurrent.duration.{Duration, DurationInt}
import scala.util.{Failure, Success}

case class Refresh(msg:Option[String])

case class Connect(host: String)

case class Connected(enumerator: Enumerator[String])

object SampleService {
  implicit val timeout = Timeout(5.seconds)

  var actors: Map[String, ActorRef] = Map.empty

  def actor(id: String):ActorRef = actors.synchronized {
    actors.find(_._1 == id).map(_._2).fold {
      val actor = Akka.system.actorOf(Props(new SampleActor(id)), name = s"Sample-$id")
      Akka.system.scheduler.schedule(0.seconds, 3.second, actor, Refresh(None))
      actors += (id -> actor)
      actor
    } (x => x)
  }

  def attach( key: String ):  (Iteratee[ String, _ ], Enumerator[ String ]) = {
    val f = (actor( key ) ? Connect( key ) ).map {
      case Connected( enumerator ) => ( Iteratee.ignore[String], enumerator )
    }
    f.onComplete{
      case Success(x) => x
      case Failure(e) => throw e
    }
    Await.result(f, Duration.Inf)
  }

}
