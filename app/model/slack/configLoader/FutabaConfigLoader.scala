package model.slack.configLoader

import com.typesafe.config.ConfigFactory

private[slack] object FutabaConfigLoader {
  lazy val defaultBaseURL = ConfigFactory.load().getString("futaba.default.baseURL")
  lazy val defaultToken = ConfigFactory.load().getString("futaba.default.token")
  lazy val defaultBotName = ConfigFactory.load().getString("futaba.default.name")
  lazy val defaultChannel = ConfigFactory.load().getString("futaba.default.channel")
}
