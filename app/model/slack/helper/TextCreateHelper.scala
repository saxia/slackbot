package model.slack.helper

case class TextCreateHelper(
                          message:String,
                          mentionMembers:Seq[String] = Seq.empty) {
  def createText(): String =
    mentionMembers.foldLeft[String]("")((x , y) => x + "@" + y + " ") + "\n" + message
}
