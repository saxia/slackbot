package model.slack.api.channels.list

import com.typesafe.config.ConfigFactory
import model.slack.api.{SlackConfiguration, MethodType}
import model.slack.api.channels.Chanel
import model.slack.configLoader.FutabaConfigLoader

@deprecated("パーサー未実装")
case class ChannelsListConfiguration(
                                      baseURL: String = FutabaConfigLoader.defaultBaseURL,
                                      token: String = FutabaConfigLoader.defaultToken,
                                      excludeArchived: Option[Boolean] = None
                                      ) extends SlackConfiguration {
  val endPoint: String = "channels.list"
  val methodType: MethodType = MethodType.Get
  type SlackObject = Seq[Chanel]

  private[slack] def makeParameters(): Seq[(String, String)] = Seq(
    "token" -> Some(token),
    "exclude_archived" -> excludeArchived.map(if (_) "1" else "0")
  ).collect { case (x, Some(y)) => (x, y)}

  //Todo:仮実装
  override private[slack] def convertToSlackObject(body: String): SlackObject = Seq()
}
