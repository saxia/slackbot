package model.slack.api

import org.apache.http.client.methods.{HttpDelete, HttpGet, HttpPost, HttpPut}

sealed abstract class MethodType(val value:String){
  private [slack] def toRequest(url:String) = this match {
    case MethodType.Get => new HttpGet(url)
    case MethodType.Post => new HttpPost(url)
    case MethodType.Put => new HttpPut(url)
    case MethodType.Delete => new HttpDelete(url)
  }
}

object MethodType{
  case object Get extends MethodType("GET")
  case object Post extends MethodType("POST")
  case object Put extends MethodType("PUT")
  case object Delete extends MethodType("DELETE")
}