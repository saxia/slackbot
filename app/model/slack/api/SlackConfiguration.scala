package model.slack.api

import model.slack.configLoader.FutabaConfigLoader

trait SlackConfiguration {
  val baseURL:String
  val token:String

  private[slack] val endPoint:String

  val methodType:MethodType
  type SlackObject

  private[slack] def makeUrl():String = baseURL + endPoint
  private[slack] def makeParameters():Seq[(String, String)]
  private[slack] def convertToSlackObject(body:String):SlackObject
}
