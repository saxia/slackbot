package model.slack.api.chat.postMessage

import model.slack.api.{SlackConfiguration, MethodType}
import model.slack.api.chat.Chat
import model.slack.configLoader.FutabaConfigLoader

case class ChatPostMessageConfiguration(
                                         text: String,
                                         baseURL:String = FutabaConfigLoader.defaultBaseURL,
                                         token: String =  FutabaConfigLoader.defaultToken,
                                         channel: String = FutabaConfigLoader.defaultChannel,
                                         username: Option[String] = Some(FutabaConfigLoader.defaultBotName)
                                         ) extends SlackConfiguration {
  private[slack] val endPoint: String = "chat.postMessage"

  private[slack] def makeParameters(): Seq[(String, String)] = Seq(
    "token" -> Some(token),
    "channel" -> Some(channel),
    "text" -> Some(text),
    "username" -> username
  ).collect { case (x, Some(y)) => (x, y)}

  override private[slack] def convertToSlackObject(body: String): SlackObject = body

  override type SlackObject = String

  val methodType: MethodType = MethodType.Post

}
