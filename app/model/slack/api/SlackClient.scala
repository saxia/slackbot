package model.slack.api

import model.slack.api.exception.SlackClientTimeOutException
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods._
import org.apache.http.client.utils.URLEncodedUtils
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import play.api.libs.concurrent.Execution.Implicits._

import scala.collection.JavaConversions._
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

case class SlackClient() {

  def executeAsync[Config<: SlackConfiguration](config:Config):Future[Either[Throwable, Config#SlackObject]] = Future{
    val nameValuePairs = config.makeParameters().map(p => new BasicNameValuePair(p._1, p._2))

    val request = config.methodType.toRequest(config.makeUrl()) match {
      case r:HttpEntityEnclosingRequestBase => r.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8")) ;r
      case r:HttpRequestBase => config.methodType.toRequest(config.makeUrl() + "?"+ URLEncodedUtils.format(nameValuePairs,"UTF-8"))
    }
    val client = HttpClientBuilder.create().build()
    Right(config.convertToSlackObject(EntityUtils.toString(client.execute(request).getEntity, "UTF-8")))
  }

  def execute[Config<: SlackConfiguration](config:Config):Either[Throwable, Config#SlackObject] = {
    val f = executeAsync(config)
    f.onComplete {
      case Success(x) => Right(x)
      case Failure(e) => Left(new SlackClientTimeOutException(e.getMessage))
    }
    Await.result(f, Duration.Inf)
  }
}
