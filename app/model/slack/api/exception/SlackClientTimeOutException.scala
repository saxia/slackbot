package model.slack.api.exception

class SlackClientTimeOutException(message:String) extends Exception(message)
