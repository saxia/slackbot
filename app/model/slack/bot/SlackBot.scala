package model.slack.bot

import java.util.UUID

import akka.actor.Cancellable
import model.slack.bot.configuration.SlackBotConfiguration
import model.slack.bot.reminderBot.{ReminderBot, ReminderBotConfiguration}

trait SlackBot[C <: SlackBotConfiguration, R <: SlackBotResult]{
  val id:String
  val config:C
  val schedule:Option[Cancellable]

  lazy val isStarted:Boolean = schedule.isDefined

  def execute():Either[Throwable, R]
  def setSchedule():SlackBot[C,R]
  def unsetSchedule():SlackBot[C,R]
}

object SlackBot{
  def apply(c:SlackBotConfiguration):SlackBot[_ <: SlackBotConfiguration, _ <: SlackBotResult] = {
    val id = UUID.randomUUID().toString()
    c match {
      case r:ReminderBotConfiguration => ReminderBot(id,r)
    }
  }
}
