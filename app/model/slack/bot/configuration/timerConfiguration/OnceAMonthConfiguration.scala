package model.slack.bot.configuration.timerConfiguration

case class OnceAMonthConfiguration(day: Int, hour: Int, minute: Int) extends TimerConfiguration
