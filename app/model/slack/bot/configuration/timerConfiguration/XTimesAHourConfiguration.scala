package model.slack.bot.configuration.timerConfiguration

case class XTimesAHourConfiguration(minutesList: Int*) extends TimerConfiguration
