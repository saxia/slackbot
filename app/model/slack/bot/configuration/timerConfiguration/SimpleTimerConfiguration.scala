package model.slack.bot.configuration.timerConfiguration

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration._

case class SimpleTimerConfiguration(
                                     override val initDelay: FiniteDuration = 1.second,
                                     override val duration: FiniteDuration = 1.minutes) extends TimerConfiguration
