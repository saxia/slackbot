package model.slack.bot.configuration.timerConfiguration

case class OnceADayConfiguration(hour: Int, minute: Int) extends TimerConfiguration
