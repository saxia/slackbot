package model.slack.bot.configuration.timerConfiguration

import scala.concurrent.duration._

trait TimerConfiguration {
  val initDelay = 1.seconds
  val duration = 1.minutes
}







