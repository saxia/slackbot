package model.slack.bot.configuration.timerConfiguration

case class OnceAHourConfiguration(minute: Int) extends TimerConfiguration
