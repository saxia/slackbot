package model.slack.bot.configuration

import model.slack.bot.configuration.timerConfiguration.TimerConfiguration

trait SlackBotConfiguration {
  val timerConfiguration:TimerConfiguration
  val name:String
}
