package model.slack.bot.reminderBot

import model.slack.api.chat.postMessage.ChatPostMessageConfiguration
import model.slack.bot.configuration.SlackBotConfiguration
import model.slack.bot.configuration.timerConfiguration.TimerConfiguration

case class ReminderBotConfiguration(
                                     name: String,
                                     timerConfiguration: TimerConfiguration,
                                     postMessageConfiguration: ChatPostMessageConfiguration,
                                     messageCustomFunction: (ChatPostMessageConfiguration) => ChatPostMessageConfiguration = c => c
                                     ) extends SlackBotConfiguration
