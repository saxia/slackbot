package model.slack.bot.reminderBot

import akka.actor.Cancellable
import model.sample.{Refresh, SampleService}
import model.slack.api.SlackClient
import model.slack.bot.SlackBot
import model.slack.bot.configuration.timerConfiguration.{OnceADayConfiguration, OnceAHourConfiguration, OnceAMonthConfiguration, XTimesAHourConfiguration}
import org.joda.time.DateTime
import play.api.libs.concurrent.Execution.Implicits._
import play.libs.Akka


case class ReminderBot(id:String, config: ReminderBotConfiguration, schedule: Option[Cancellable] = None)
  extends SlackBot[ReminderBotConfiguration, ReminderBotResult] {

  private[this] def sendSlackMessage(): Either[Throwable, ReminderBotResult] = {
    SlackClient().execute(config.messageCustomFunction(config.postMessageConfiguration)) match {
      case Left(e) => {
        SampleService.actors("log") ! Refresh(Some(s"${config.name}:${e.getMessage}"))
        Left(e)
      }
      case Right(s) => {
        SampleService.actors("log") ! Refresh(Some(s"${config.name}:$s"))
        Right(SendReminderBotResult(s))
      }
    }
  }

  private[this] def canSend(now: DateTime): Boolean = {

    //    import akka.actor.{ActorSystem,Props}
    //    val system = ActorSystem("system")
    //    val actor = system.actorOf(Props[SampleActor])
    //    actor ! s"${DateTime.now().toString("yyyy/MM/dd hh:MM:ss")} だよもん"

    SampleService.actors("log") ! Refresh(Some(s"${config.name}:時間監視中"))
    config.timerConfiguration match {
      case x: XTimesAHourConfiguration =>
        x.minutesList.contains(now.getMinuteOfHour)
      case OnceAHourConfiguration(m) =>
        now.getMinuteOfHour == m
      case OnceADayConfiguration(h, m) =>
        now.getHourOfDay == h && now.getMinuteOfHour == m
      case OnceAMonthConfiguration(d, h, m) =>
        now.getDayOfMonth == d && now.getHourOfDay == h && now.getMinuteOfHour == m
    }
  }

  def execute(): Either[Throwable, ReminderBotResult] = if (canSend(DateTime.now())) {
    sendSlackMessage()
  } else {
    Right(NotSendReminderBotResult())
  }

  def setSchedule(): SlackBot[ReminderBotConfiguration, ReminderBotResult] = schedule.fold(
    copy(schedule = Some(
      Akka.system.scheduler.schedule(config.timerConfiguration.initDelay, config.timerConfiguration.duration) {
        execute()
      }
    ))
  )(_ => this)

  def unsetSchedule(): SlackBot[ReminderBotConfiguration, ReminderBotResult] = schedule.fold(this)(c => {
    c.cancel()
    copy(schedule = None)
  })
}
