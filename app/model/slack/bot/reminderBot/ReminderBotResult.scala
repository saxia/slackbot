package model.slack.bot.reminderBot

import model.slack.bot.SlackBotResult

trait ReminderBotResult extends SlackBotResult

case class SendReminderBotResult(sendResult:String) extends ReminderBotResult

case class NotSendReminderBotResult() extends ReminderBotResult