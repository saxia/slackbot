import play.api.{Application, GlobalSettings}

trait Global extends GlobalSettings{
  override def onStart(app: Application): Unit = super.onStart(app)
}
