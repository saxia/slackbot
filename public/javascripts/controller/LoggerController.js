var futabaApp = angular.module('futabaApp', [])

futabaApp.controller('LoggerController', function ($scope) {
    $scope.messages = [];

    var socket = new WebSocket("ws://localhost:9000/log");
    socket.open = function () {
        socket.send("hello");
    };
    socket.onclose = function () {
    };
    socket.onmessage = function (e) {
        $scope.$apply(function () {
            $scope.messages.push(e.data)
        })
    };
    $scope.socket = socket;

    $scope.send = function (message) {
        $scope.socket.send(message)
    }
});