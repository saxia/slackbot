package model.slack.api

import com.typesafe.config.ConfigFactory
import model.slack.api.channels.list.ChannelsListConfiguration
import model.slack.api.chat.postMessage.ChatPostMessageConfiguration
import org.specs2.mutable.Specification

class SlackClientSpec extends Specification{
  val token = ConfigFactory.load().getString("futaba.default.token")
  "ChanelListAccess" should {
    "アクセスできる" in {
      val actual = SlackClient().execute(ChannelsListConfiguration(token))
      println(actual.right.get)
      actual must beRight
    }
  }
  "chat.postMessage" should {
    "アクセスできる" in {
      val actual = SlackClient().execute(ChatPostMessageConfiguration(token = token,text = "@y_harada てすとだよー"))
      println(actual.right.get)
      actual must beRight
    }
  }
}
